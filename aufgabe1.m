[temperaturen, einheit] = monatswerte('Temperaturen.txt');

temperaturen

switch einheit
    case 'C'
        einheit = 'celcius';
    case 'F'
        einheit = 'fahrenheit';
end

filename = [ 'temperaturen_' einheit ];
save( [filename '.mat'], 'temperaturen', '-mat' )
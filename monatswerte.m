function [werte, einheit] = monatswerte(pfad)
%MONATSWERTE Wertet Datei mit Wetterdaten aus
%   Die Datei muss pro Spalte je ein Monat enthalten, Temperaturen in Kelvin.
%   Die Funktion gibt den Minimal-, Maximal- und Mitteltemperatur für jedes
%   Monat als Array zurück. Die erste Zeile enthält die Nummer des Monats,
%   die weiteren Zeilen die Werte in der genannten Reihenfolge. Die Einheit
%   wird durch Nutzereingabe festgelegt und als weitere Variable mit
%   zurückgegeben.

einheit = '';
while ~( strcmp(einheit,'C') || strcmp(einheit, 'F') )
   einheit = input('Bitte geben Sie eine Einheit [C/F] ein: ', 's'); 
end

fileID = fopen(pfad,'r');
fileContent = fscanf( fileID, '%f', [12, Inf]);
fclose(fileID);

werte = zeros(4,12);
for j = 1:12
    werte(1, j) = j; % Nummer des Monats
    werte(2, j) = min( fileContent(j,:) ); % Minimum
    werte(3, j) = max( fileContent(j,:) ); % Maximum
    werte(4, j) = mean( fileContent(j,:), 'omitnan' ); % Durchschnitt, NaN werden verworfen
end

switch einheit % Temperaturen (= Werte in den Zeilen 2 bis 4) in die gewünschte Einheit konvertieren
    case 'C'
       werte(2:4, :) = arrayfun( @(x)convertKelvinToCelcius(x), werte(2:4, :) );
    case 'F'
       werte(2:4, :) = arrayfun( @(x)convertKelvinToFahrenheit(x), werte(2:4, :) );
end

end % function monatswerte

function [celcius] = convertKelvinToCelcius(kelvin)
    celcius = kelvin - 273.15;
end

function [fahrenheit] = convertKelvinToFahrenheit(kelvin)
    fahrenheit = (kelvin - 273.15) * 9/5 + 32;
end


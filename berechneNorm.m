function [norm] = berechneNorm(vectorOrMatrix)
%BERECHNENORM Berechnet die Norm eines Vektors oder einer Matrix
%   für Vektoren wird die euklidsche Norm zurückgegeben
%   für Matrizen die höchste Summe der Beträge der Zeilenelemente

if isvector(vectorOrMatrix) % Vektor
    norm = euclideanNorm(vectorOrMatrix);
else % Matrix
    norm = matrixNorm(vectorOrMatrix);
end

end

function [norm] = euclideanNorm(vector)
    sqsum = 0;
    for i = 1:numel(vector)
    	sqsum = sqsum + vector(i)^2;
    end
    norm = sqrt( sqsum );
end

function [norm] = matrixNorm(matrix)
    rowSums = zeros( size(matrix, 1), 1 );
    for i = 1:size(matrix, 1)
        for j = 1:size(matrix, 2)
            rowSums(i) = rowSums(i) + abs( matrix(i,j) );
        end
    end
    norm = max(rowSums);
end



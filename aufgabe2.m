v1 = [1 1 0]
berechneNorm( v1 )
v2 = [1; 0; 2]
berechneNorm( v2 )
v3 = rand(1000,1);
A1 = [1 1 4; 0 -1 7]
berechneNorm( A1 )
A2 = [1 1; 5 -3]
berechneNorm( A2 )
A3 = rand(1000, 1000);

berechneNorm(v3) - norm(v3)
berechneNorm(A3) - norm(A3) %  2-norm or maximum singular value of matrix X
berechneNorm(A3) - norm(A3, Inf) % p-norm, maximum absolute row sum